/**
 * \file Pos3D.h
 * \brief Header du fichier contenant les opérations arithmétiques sur les positions 3D.
 * \authors Emile Eid
 * \authors Camille Idatte
 * \authors Nathan Piranda
 */

#ifndef ROLLERCOASTER_POS3D_H
#define ROLLERCOASTER_POS3D_H


#include "CH3D.h"

/**
 * \class Pos3D
 * \brief Classe des coordonnées homogènes.
 */
class Pos3D : public CH3D {

public:
    Pos3D(void);
    Pos3D(double px, double py, double pz);
    Pos3D(Pos3D *p);

    ~Pos3D(void);

    double distance(Pos3D *pD);

    static double distance(Pos3D *p1, Pos3D *p2);

};


#endif //ROLLERCOASTER_POS3D_H
