/**
 * \file Dir3D.h
 * \brief Header du fichier contenant les opérations arithmétiques sur les directions 3D.
 * \authors Emile Eid
 * \authors Camille Idatte
 * \authors Nathan Piranda
 */

#ifndef ____DIR3D____
#define ____DIR3D____


class Pos3D;

#include "CH3D.h"

/**
 * \class Dir3D
 * \brief Classe des directions 3D..
 */
class Dir3D : public CH3D {

public:

	/* Constructeurs                            */

	Dir3D(void);
	Dir3D(double x, double y, double z);
	Dir3D(Dir3D *c);
	Dir3D(Pos3D *pi, Pos3D *pf);

	/* Destructeur                              */

	~Dir3D(void);

	/* Methode de calcul de la norme de this    */

	double norme(void);

	/* Methode de normalisation de this         */

	void normalisation(void);

	/* Methode de calcul du produit scalaire    */
	/* de this et de la direction d             */

	double produitScalaire(Dir3D *d);

	/* Methode statique de calcul               */
	/* du produit scalaire des deux directions  */
	/* d1 et d2                                 */

	static double produitScalaire(Dir3D *d1, Dir3D *d2);

	/* Methode de calcul du produit vectoriel   */
	/* de this par la direction d               */
	/* avec stockage du resultat dans this      */

	void produitVectoriel(Dir3D *d);

	/* Methode de calcul du produit vectoriel   */
	/* de this par la direction d               */
	/* avec stockage du resultat dans res       */

	void produitVectoriel2(Dir3D *d, Dir3D *res);

	/* Methode statique de calcul               */
	/* du produit vectoriel de deux directions  */
	/* d1 et d2                                 */
	/* Retour d'un objet Dir3D                  */
	/* cree dans la methode                     */

	static Dir3D *produitVectoriel(Dir3D *d1, Dir3D *d2);

	/* Methode statique de calcul               */
	/* du produit vectoriel de deux directions  */
	/* d1 et d2 avec Stockage du resultat       */
	/* dans res                                 */

	static void produitVectoriel(Dir3D *d1, Dir3D *d2, Dir3D *res);
};

#endif
