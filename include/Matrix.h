/**
 * \file Matrix.h
 * \brief Header contenant les différentes matrices de transformations.
 * \authors Emile Eid
 * \authors Camille Idatte
 * \authors Nathan Piranda
 */

#ifndef ROLLERCOASTER_MATRIX_H
#define ROLLERCOASTER_MATRIX_H

#include <math.h>
typedef float matrix[4][4];

static matrix mBezier =
        { -1.0F, 3.0F,-3.0F, 1.0F,
          3.0F,-6.0F, 3.0F, 0.0F,
          -3.0F, 3.0F, 0.0F, 0.0F,
          1.0F, 0.0F, 0.0F, 0.0F };

static matrix mNURBS =
        { -0.1666666F, 0.5F,      -0.5F,      0.1666666F,
          0.5F      ,-1.0F,       0.5F,      0.0F,
          -0.5F      , 0.0F,       0.5F,      0.0F,
          0.1666666F, 0.6666666F, 0.1666666F,0.0F };

static matrix mCatmullRom =
        { -0.5F, 1.5F,-1.5F, 0.5F,
          1.0F,-2.5F, 2.0F,-0.5F,
          -0.5F, 0.0F, 0.5F, 0.0F,
          0.0F, 1.0F, 0.0F, 0.0F };

#endif //ROLLERCOASTER_MATRIX_H
