//
// Created by Nathan Piranda on 2019-04-11.
//

#ifndef ROLLERCOASTER_ENVIRONMENT_H
#define ROLLERCOASTER_ENVIRONMENT_H


#if defined(WIN32) || defined(WIN64)
#include <GL/glut.h>
#endif

#ifdef __APPLE__
#include <GLUT/glut.h>
#endif

void drawFloor(float lx,float lz,int nx,int nz);

#endif //ROLLERCOASTER_ENVIRONMENT_H
