/**
 * \file CH3D.h
 * \brief Header du fichier contenant les opérations arithmétiques sur les coordonnées 3D
 * \authors Emile Eid
 * \authors Camille Idatte
 * \authors Nathan Piranda
 */

#ifndef ROLLERCOASTER_COORD3D_H
#define ROLLERCOASTER_COORD3D_H

/**
 * \class CH3D
 * \brief Classe des coordonnées homogènes.
 */
class CH3D  {

public :
    /* Coordonnees                            */
    union {
        struct{
            double x;
            double y;
            double z;
            double w;
        };
        double c[4];
    };

public :
    /* Constructeurs                            */
    CH3D(void);
    CH3D(double x,double y,double z,double t);
    CH3D(CH3D *clone);

    /* Destructeur                              */
    ~CH3D(void);

    /* Methode d'affichage texte                */
    void print(void);
};


#endif //ROLLERCOASTER_COORD3D_H
