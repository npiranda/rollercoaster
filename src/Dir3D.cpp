/**
 * \file Dir3D.cpp
 * \brief Ce fichier contient les opérations arithmétiques sur les directions 3D.
 * \authors Emile Eid
 * \authors Camille Idatte
 * \authors Nathan Piranda
<<<<<<< HEAD
 */


#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "../include/CH3D.h"
#include "../include/Dir3D.h"
#include "../include/Pos3D.h"

/* Constructeurs                                */

Dir3D::Dir3D(void) :CH3D(0.0, 0.0, 0.0, 0.0) {
}

Dir3D::Dir3D(double x, double y, double z) : CH3D(x, y, z, 0.0) {
}

Dir3D::Dir3D(Dir3D *p) : CH3D(p) {
}

Dir3D::Dir3D(Pos3D *pi, Pos3D *pf) : CH3D() {
	c[0] = pf->c[0] - pi->c[0];
	c[1] = pf->c[1] - pi->c[1];
	c[2] = pf->c[2] - pi->c[2];
	c[3] = 0.0;
}

/* Destructeur                                  */

Dir3D::~Dir3D(void) {
}

/* Methode de calcul de la norme de this        */

double Dir3D::norme(void) {
	return(sqrt(c[0] * c[0] + c[1] * c[1] + c[2] * c[2]));
}

/* Methode de normalisation de this             */

void Dir3D::normalisation(void) {
	double d = norme();
	if (d != 0.0) {
		c[0] /= d;
		c[1] /= d;
		c[2] /= d;
	}
}

/* Methode de calcul du produit scalaire        */
/* de this et de la direction d                 */

double Dir3D::produitScalaire(Dir3D *d) {
	return(produitScalaire(this, d));
}

/* Methode statique de calcul                   */
/* du produit scalaire des deux directions      */
/* d1 et d2                                     */

double Dir3D::produitScalaire(Dir3D *d1, Dir3D *d2) {
	return(d1->c[0] * d2->c[0] + d1->c[1] * d2->c[1] + d1->c[2] * d2->c[2]);
}

/* Methode de calcul du produit vectoriel       */
/* de this par la direction d                   */
/* avec stockage du resultat dans this          */

void Dir3D::produitVectoriel(Dir3D *d) {
	produitVectoriel(this, d, this);
}

/* Methode de calcul du produit vectoriel       */
/* de this par la direction d                   */
/* avec stockage du resultat dans res           */

void Dir3D::produitVectoriel2(Dir3D *d, Dir3D *res) {
	produitVectoriel(this, d, res);
}

/* Methode statique de calcul                   */
/* du produit vectoriel de deux directions      */
/* d1 et d2                                     */
/* Retour d'un objet Dir3D                      */
/* cree dans la methode                         */

Dir3D *Dir3D::produitVectoriel(Dir3D *d1, Dir3D *d2) {
	Dir3D *d = new Dir3D();
	produitVectoriel(d1, d2, d);
	return d;
}

/* Methode statique de calcul                   */
/* du produit vectoriel de deux directions      */
/* d1 et d2 avec Stockage du resultat           */
/* dans res                                     */

void Dir3D::produitVectoriel(Dir3D *d1, Dir3D *d2, Dir3D *res) {
	double x = d1->c[1] * d2->c[2] - d1->c[2] * d2->c[1];
	double y = d1->c[2] * d2->c[0] - d1->c[0] * d2->c[2];
	double z = d1->c[0] * d2->c[1] - d1->c[1] * d2->c[0];
	res->c[0] = x;
	res->c[1] = y;
	res->c[2] = z;
	res->c[3] = 0.0;
}
