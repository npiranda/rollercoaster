/**
 * \file Scene.cpp
 * \brief Ce fichier exécute la scène 3D du projet.
 * \authors Emile Eid
 * \authors Camille Idatte
 * \authors Nathan Piranda
 */

/**
 * \mainpage Projet Grand Huit
 * Projet du module Informatique Graphique \n
 * M1 Informatique ISL - UFR ST Besançon 2018/2019 \n
 * Emile Eid - Camille Idatte - Nathan Piranda
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "../include/Pos3D.h"
#include "../include/Dir3D.h"
#include "../include/Environment.h"


#if defined(WIN32) || defined(WIN64)
#include <GL/gl.h>
#include <GL/glut.h>
//#include "../PNG/windows/ChargePngFile.h"
#include "../../solution_rollercoaster/Src/PNG/ChargePngFile.h"
#define M_PI 3.14159265358979323846
#endif

#ifdef __APPLE__
#include <GLUT/glut.h>
#include <cstdlib>
#include "../PNG/ChargePngFile.h"
#endif

#include <string>
#include <vector>


static float rx = 0.0F;
static float ry = 0.0F;
static float rz = 0.0F;
static int f1;
static int f2;
static bool isInit = false;
static bool fps = false;

Pos3D currentTeapotPosition;
Pos3D currentTargetPosition;
Pos3D currentTopPosition;

static int nb = 240;
static int aff = 0;
float section = 0.0;

static double distanceTotale = 0.0;
static int nbPointVitesse = 0;

int rotate = 0.0F;				// Rotation des roues

static unsigned int textureGrass;

static const float white[4]= {1.0,1.0,1.0,0.8};
static const float blanc[] = { 1.0F,1.0F,1.0F,1.0F };
static const float noir[] = { 0.0F,0.0F,0.0F,1.0F };
static const float couleurBrouillard[] = { 0.85F,0.85F,0.85F,1.0F };
static const float ours[4] = { 0.145f,0.060f,0.035f,1.0f };
static const float gris[4] = { 0.115f,0.115f,0.115f,1.0f };
static const float beige[4] = { 0.222f,0.188f,0.153f,1.0f };
static const float wagon[4] = { 0.0f,0.0f,0.50f,1.0f };
static const float jaune[] = { 1.0F,1.0F,0.0F,1.0F };


static int nbPoints = 59;
static Pos3D *bfPos[] = { new Pos3D(12.0, 0.0, 2.00), new Pos3D(12.0, 0.0,-3.00), new Pos3D(12.0, 3.0,-8.00),
                          new Pos3D(10.0, 7.0,-12.0), new Pos3D(7.0, 11.0,-15.0), new Pos3D(2.0, 13.0,-16.0),
                          new Pos3D(-2.0, 12.0,-15.0), new Pos3D(-5.0, 9.0,-13.00), new Pos3D(-5.0, 5.0,-10.00),
                          new Pos3D(-5.0, 2.0,-6.0), new Pos3D(-5.0, 1.0,-2.00), new Pos3D(-5.0, 1.0, 2.00),
                          new Pos3D(-5.0, 2.0, 5.5), new Pos3D(-5.0, 4.0, 7.20), new Pos3D(-5.0, 7.5, 6.00),
                          new Pos3D(-4.0, 8.5, 3.0), new Pos3D(-3.0, 7.0, 0.50), new Pos3D(-3.0, 4.0, 0.00),
                          new Pos3D(-3.0, 1.0, 2.0), new Pos3D(-3.0, 1.0, 6.00), new Pos3D(-3.0, 2.0, 10.00),
                          new Pos3D(-4.0, 3.0, 14.0), new Pos3D(-9.0, 4.0, 15.00), new Pos3D(-13.0, 6.0, 12.00),
                          new Pos3D(-15.0, 8.0, 9.0), new Pos3D(-15.0, 8.0, 2.00), new Pos3D(-15.0, 7.0,-5.00),
                          new Pos3D(-13.0, 6.0,-8.0), new Pos3D(-10.0, 3.0,-11.00), new Pos3D(-7.0, 1.0,-13.00),
                          new Pos3D(-2.0, 0.0,-14.0), new Pos3D( 2.0, 0.0,-14.00), new Pos3D( 5.0, 2.0,-12.00),
                          new Pos3D( 5.0, 5.0,-9.0), new Pos3D( 5.0, 6.0,-6.00), new Pos3D( 6.0, 4.0,-3.00),
                          new Pos3D( 6.0, 1.0,-1.0), new Pos3D( 4.0, 1.0, 2.00), new Pos3D( 1.0, 2.0, 4.00),
                          new Pos3D(-2.0, 5.0, 4.0), new Pos3D(-5.0, 5.0, 4.00), new Pos3D(-8.0, 3.0, 5.00),
                          new Pos3D(-10.0, 1.0, 7.0), new Pos3D(-10.0, 0.0, 11.00), new Pos3D(-7.0, 0.0, 14.00),
                          new Pos3D(-2.0, 0.0, 16.0), new Pos3D( 2.0, 0.0, 15.00), new Pos3D( 6.0, 1.0, 14.00),
                          new Pos3D( 9.0, 1.0, 14.0), new Pos3D( 13.0, 2.0, 14.00), new Pos3D( 14.0, 4.0, 17.00),
                          new Pos3D( 12.0, 6.0, 18.0), new Pos3D( 9.0, 5.0, 17.00), new Pos3D( 8.0, 3.0, 15.00),
                          new Pos3D( 9.0, 2.0, 11.0), new Pos3D( 11.0, 1.0, 7.00),
                          new Pos3D(12.0, 0.0, 2.00), new Pos3D(12.0, 0.0,-3.00), new Pos3D(12.0, 3.0,-8.00) };


static Pos3D *bfPosVerticale[] = { new Pos3D(12.0, 1.0, 2.00), new Pos3D(12.0, 1.0,-3.00), new Pos3D(12.0, 4.0,-8.00),
                                   new Pos3D(10.0, 8.0,-12.0), new Pos3D(7.0, 12.0,-15.0), new Pos3D(2.0, 14.0,-16.0),
                                   new Pos3D(-2.0, 13.0,-15.0), new Pos3D(-5.0, 10.0,-13.00), new Pos3D(-5.0, 6.0,-10.00),
                                   new Pos3D(-5.0, 3.0,-6.0), new Pos3D(-5.0, 2.0,-2.00), new Pos3D(-5.0, 2.0, 2.00),
                                   new Pos3D(-5.0, 3.0, 5.5), new Pos3D(-5.0, 5.0, 5.70), new Pos3D(-5.0, 6.5, 6.00),
                                   new Pos3D(-4.0, 7.5, 2.0), new Pos3D(-3.0, 6.0, 0.50), new Pos3D(-3.0, 5.0, 1.00),
                                   new Pos3D(-3.0, 2.0, 2.0), new Pos3D(-3.0, 2.0, 6.00), new Pos3D(-3.0, 3.0, 10.00),
                                   new Pos3D(-4.0, 4.0, 14.0), new Pos3D(-9.0, 5.0, 15.00), new Pos3D(-13.0, 7.0, 12.00),
                                   new Pos3D(-15.0, 9.0, 9.0), new Pos3D(-15.0, 9.0, 2.00), new Pos3D(-15.0, 8.0,-5.00),
                                   new Pos3D(-13.0, 7.0,-8.0), new Pos3D(-10.0, 4.0,-11.00), new Pos3D(-7.0, 2.0,-13.00),
                                   new Pos3D(-2.0, 1.0,-14.0), new Pos3D( 2.0, 1.0,-14.00), new Pos3D( 5.0, 3.0,-12.00),
                                   new Pos3D( 5.0, 6.0,-9.0), new Pos3D( 5.0, 7.0,-6.00), new Pos3D( 6.0, 5.0,-3.00),
                                   new Pos3D( 6.0, 3.0,-1.0), new Pos3D( 4.0, 3.0, 2.00), new Pos3D( 1.0, 4.0, 4.00),
                                   new Pos3D(-2.0, 6.0, 4.0), new Pos3D(-5.0, 6.0, 4.00), new Pos3D(-8.0, 4.0, 5.00),
                                   new Pos3D(-10.0, 2.0, 7.0), new Pos3D(-10.0, 1.0, 11.00), new Pos3D(-7.0, 1.0, 14.00),
                                   new Pos3D(-2.0, 1.0, 16.0), new Pos3D( 2.0, 1.0, 15.00), new Pos3D( 6.0, 2.0, 14.00),
                                   new Pos3D( 9.0, 2.0, 14.0), new Pos3D( 13.0, 3.0, 14.00), new Pos3D( 14.0, 5.0, 17.00),
                                   new Pos3D( 12.0, 7.0, 18.0), new Pos3D( 9.0, 6.0, 17.00), new Pos3D( 8.0, 4.0, 15.00),
                                   new Pos3D( 9.0, 3.0, 11.0), new Pos3D( 11.0, 2.0, 7.00),
                                   new Pos3D(12.0, 1.0, 2.00), new Pos3D(12.0, 1.0,-3.00), new Pos3D(12.0, 4.0,-8.00) };

Pos3D **splinePointsVerticales;

Pos3D **splinePoints;
Pos3D **posVitessePoints;

static double distancePos(Pos3D *p0, Pos3D *p1) {
    return p0->distance(p1);
}


static double CATMULL_ROM[4][4] = { { -1.0/2.0,  3.0/2.0, -3.0/2.0,  1.0/2.0 },
                                    {  2.0/2.0, -5.0/2.0,  4.0/2.0, -1.0/2.0 },
                                    { -1.0/2.0,      0.0,  1.0/2.0,      0.0 },
                                    {      0.0,  2.0/2.0,      0.0,      0.0 } };


static void postRedisplay(void) {
    glutPostWindowRedisplay(f1);
    glutPostWindowRedisplay(f2);
}

/**
 * @param r Le rayon du cylindre
 * @param h La hauteur du cylindre
 * @param d1 Nombre de facette du cylindre
 * @brief Dessine un cylindre
 */
void drawCylinder(float r,float h,int d1) {
    glBegin(GL_QUAD_STRIP);
    float x0, y0;
    double a;
    for (int i=0; i<=d1; i++) {
        a = 2.0*i*M_PI/d1;
        x0 = r*cos(a);
        y0 = r*sin(a);

        glNormal3f(x0,y0,0);

        glVertex3f(x0,y0,0);
        glVertex3f(x0,y0,h);
    }
    glEnd();
}


/**
 * @param p0 Position courante
 * @param p1 Position prochaine
 * @param pointSize Taille du point
 * @brief Dessine un morceau de B-Splines
 */
void drawPoint(Pos3D *p0, Pos3D *p1, double pointSize) {
    static const float red[4]= {1.0,0.0,0.0,1.0};
    static const float silver[4]= {0.192,0.192,0.192,1.0};

    glPushMatrix();

    glMaterialfv(GL_FRONT,GL_AMBIENT_AND_DIFFUSE,silver);
    glTranslated(p0->x,p0->y,p0->z);
    glutSolidSphere(pointSize,10,10);

    // Calcul du vecteur entre le point courant et le point suivant :
    Pos3D v(p1->x-p0->x,p1->y-p0->y,p1->z-p0->z);

    double d = Pos3D::distance(p0,p1);

    // Normalisation du vecteur :
    v.x /= d;
    v.y /= d;
    v.z /= d;

    // Calcul des coordonnées sphériques :
    double phi = asin(v.z);
    double theta = (v.x>0)?asin(v.y/cos(phi)):M_PI-asin(v.y/cos(phi));
    glRotated(theta*180.0/M_PI,0,0,1.0);
    glRotated(90.0-phi*180.0/M_PI,0,1.0,0);
    glMaterialfv(GL_FRONT,GL_AMBIENT_AND_DIFFUSE,silver);
    drawCylinder(pointSize, d, 24);
    //drawRail(pointSize,d,24);

    glPopMatrix();
}

/**
 * @param positionSet
 * @param t Variation de la position
 * @param matrix Matrice de Catmull-Rom
 * @param position Position sur la B-Splines
 * @brief Donne la position sur une B-Spline en fonction de t
 */
void determinationPositionSurBSpline(Pos3D **positionSet,double t,double matrix[4][4],Pos3D *position) {
    double vt[4] = { t*t*t,t*t,t,1.0 };
    double vtmb[4] = { 0.0,0.0,0.0,0.0 };
    for ( int j = 0 ; j < 4 ; j++ ) {
        for ( int k = 0 ; k < 4 ; k++ )
            vtmb[j] += vt[k] * matrix[k][j] ; }
    position->x = position->y = position->z = 0.0;
    for ( int j = 0 ; j < 4 ; j++ ) {
        position->x += vtmb[j] * positionSet[j]->x ;
        position->y += vtmb[j] * positionSet[j]->y ;
        position->z += vtmb[j] * positionSet[j]->z ; }
}

/**
 * @param positionSet
 * @param n
 * @param matrix
 * @param nbPoints
 * @param generatedPositionsSet
 */
void calculBSpline(Pos3D **positionSet,int n,double matrix[4][4],int nbPoints,Pos3D **generatedPositionsSet) {
    for ( int i = 0 ; i < nbPoints ; i++ ) {
        double pos = i/(nbPoints-1.0)*(n-3);
        int nb =(int) pos;
        if ( nb == n-3 )
            nb = n-4;
        double t = pos-nb;
        determinationPositionSurBSpline(&positionSet[nb],t,matrix,generatedPositionsSet[i]);
    }
}

/**
 * @param tabVitesse tableau contenant les positions équidistante
 * @param i actuelle avec un indice
 * @param tailleTab taille du tableau de positions
 * @param multiplicateur Permet d'augmenter ou de diminuer les fluctuations de vitesse de base laisser à 1 si cela parait cohérent
 * @return vitesse obtenue durant la section
 * @brief fonction permettant de calculer une vitesse par rapport à la position actuelle et la suivante, et un multiplicateur.
 */
double calculVitesse(Pos3D **tabVitesse, int i, int tailleTab, int multiplicateur) {
    double g = 9.80665;
    double res;
    int j = i + 1;
    if (i+1 == tailleTab) {
        j = 0;
    }
    res = ((tabVitesse[i]->y - tabVitesse[j]->y)*multiplicateur)*g;
    if (res > 0) {
        res = sqrt(res);
    }
    else {
        res = -res;
        res = sqrt(res);
        res = -res;
    }
    return res;
}

/**
 * @param n nombre de case à allouer
 * @return un tableau de Pos3D** avec l'espace mémoire alloué correspondant
 */
Pos3D **allocationPos3D(int n) {
    Pos3D **tP =(Pos3D **) calloc(n,sizeof(Pos3D *));
    for ( int i = 0 ; i < n ; i++ )
        tP[i] = new Pos3D();
    return tP;
}

/**
 * @param tP tableau de Pos3D** à désallouer
 * @param n nombre d'élément dans le tableau
 * @brief désalloue l'espace mémoire d'un tableau tP de Pos3D de taille n
 */
void desallocationPos3D(Pos3D **tP,int n) {
    for ( int i = 0 ; i < n ; i++ )
        delete(tP[i]);
    free(tP);
}

void special(int key,int x,int y) {
    switch (key) {
        case GLUT_KEY_UP :
            rx++;
            postRedisplay();
            break;
        case GLUT_KEY_DOWN :
            rx--;
            postRedisplay();
            break;
        case GLUT_KEY_LEFT :
            ry++;
            postRedisplay();
            break;
        case GLUT_KEY_RIGHT :
            ry--;
            postRedisplay();
            break;
        case GLUT_KEY_PAGE_UP :
            switch (glutGetModifiers()) {
                case GLUT_ACTIVE_ALT :
                    nb++;
                    postRedisplay();
                    break;
                default :
                    rz++;
                    postRedisplay(); }
            break;
        case GLUT_KEY_PAGE_DOWN :
            switch (glutGetModifiers()) {
                case GLUT_ACTIVE_ALT :
                    nb--;
                    if ( nb == 1 )
                        nb = 2;
                    postRedisplay();
                    break;
                default :
                    rz--;
                    postRedisplay(); }
            break; }
}

/**
 *
 * @param p0 Point de départ de la bspline
 * @param p1 Point suivant de la bspline
 * @param retenu distance a ajouter au premier point
 * @param distance La distance entre chaque point equidistant
 * @param result tableau de point equidistant
 * @param nbPointVitesse nombre de point dans le tableau de point equidistant
 * @param current point actuel à attribuer dans le tableau
 * @return la distance parcouru en trop entre p0 et p1 et deviendra la retenu pour le prochain appel de fonction
 * @brief Permet d'initialiser un tableau de point equidistant à partir de deux points, d'unce distance et d'une retenue
 */
double placerPointVitesse(Pos3D *p0, Pos3D *p1, double retenu, double distance, Pos3D **result, int nbPointVitesse, int *current) {
    int i = *(current);
    Dir3D direction = new Dir3D(p0, p1);
    double dist = p0->distance(p1);

    double distCurrent = retenu;
    double nbPointDouble = ((dist - retenu) / distance);
    int j = 0;

    result[i] = new Pos3D(p0->x + direction.x*retenu, p0->y + direction.y*retenu, p0->z + direction.z*retenu);

    i++;
    j++;

    while (distCurrent < dist && i < nbPointVitesse)
    {
        distCurrent += distance;
        result[i] = new Pos3D(result[i - 1]->x + direction.x/nbPointDouble, result[i - 1]->y + direction.y/nbPointDouble, result[i - 1]->z + direction.z/nbPointDouble);

        i++;
        j++;
    }

    if (j >= nbPointDouble) {
        i--;
    }
    if (distCurrent > dist) {
        retenu = distCurrent - dist;
    }
    *(current) = i;
    return retenu;
}

/**
 * @brief Permet d'initialiser une lumière ambiante blanche
 */
void lumiereAmbiante() {
    const GLfloat pos[] = { 0.0,0.0,0.0,0.0 };
    glLightfv(GL_LIGHT7, GL_POSITION, pos);
    glLightfv(GL_LIGHT7, GL_DIFFUSE, noir);
    glLightfv(GL_LIGHT7, GL_SPECULAR, noir);
    glLightfv(GL_LIGHT7, GL_AMBIENT, blanc);
    glEnable(GL_LIGHT7);
}

static void init(void) {
    glClearColor(0.52F, 0.80F, 0.92F, 1.0F);
    static const float ambientColor[4] = {0.3f, 0.3f, 0.3f, 1.0f};
    static const float diffuseColor[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    static const float specularColor[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glEnable(GL_LIGHTING);
    glEnable(GL_SMOOTH);

    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambientColor);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseColor);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specularColor);

    if (fps) {
        printf("FOG\n");
        glEnable(GL_FOG);
        glFogi(GL_FOG_MODE, GL_EXP2);
        glFogfv(GL_FOG_COLOR, couleurBrouillard);
        glFogf(GL_FOG_DENSITY, 0.3F);
        glHint(GL_FOG_HINT, GL_DONT_CARE);
    }
    fps = true;

    lumiereAmbiante();

    splinePoints = allocationPos3D(nb);
    splinePointsVerticales = allocationPos3D(nb);

    calculBSpline(bfPos, nbPoints, CATMULL_ROM, nb, splinePoints);
    calculBSpline(bfPosVerticale,nbPoints,CATMULL_ROM,nb,splinePointsVerticales);

    for (int i = 0; i < nb - 1; i++) {
        distanceTotale += distancePos(splinePoints[i], splinePoints[i + 1]);
    }

    if (!isInit) {
        nbPointVitesse = nb*2;
        double distEntre = (distanceTotale / (double) nbPointVitesse);
        printf("dist entre : %f\n", distEntre);
        posVitessePoints = allocationPos3D(nbPointVitesse);


        double retenu = 0.0;
        int current = 0;


        for (int i = 0; i < nb - 1; i++) {
            retenu = placerPointVitesse(splinePoints[i], splinePoints[i + 1], retenu, distEntre, posVitessePoints,
                                        nbPointVitesse, &current);

        }

        isInit = true;
    }

    int rx2, ry2;
    unsigned char *img = chargeImagePng("../bin/Skybox.png", &rx2, &ry2);


    if (img) {
        glTexImage2D(GL_TEXTURE_2D, 0, 3, rx2, ry2, 0, GL_RGB, GL_UNSIGNED_BYTE, img);
        free(img);
        printf("Skybox texture successfully loaded\n");
    } else {
        printf("Skybox texture not loaded\n");
    }

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

}

/**
* @param ct longueur du côté du cube
* @brief Permet de créer un cube de côté ct. Principalement utilisé pour palcer les textures de la skybox
*/
static void mySolidCubeAvecNormales(double ct) {
    float c = (float)ct / 2.0F;
    glPushMatrix();
    glBegin(GL_QUADS);

    //front
    { glNormal3f(0.0F, 0.0F, -1.0F);
        glTexCoord2f(0.50F, 0.66F);
        glVertex3f(c, c, -c);
        glTexCoord2f(0.50F, 0.34F);
        glVertex3f(c, -c, -c);
        glTexCoord2f(0.25F, 0.34F);
        glVertex3f(-c, -c, -c);
        glTexCoord2f(0.25F, 0.66F);
        glVertex3f(-c, c, -c); }
    //back
    { glNormal3f(0.0F, 0.0F, 1.0F);
        glTexCoord2f(0.75F, 0.66F);
        glVertex3f(c, c, c);
        glTexCoord2f(1.0F, 0.66F);
        glVertex3f(-c, c, c);
        glTexCoord2f(1.0F, 0.34F);
        glVertex3f(-c, -c, c);
        glTexCoord2f(0.75F, 0.34F);
        glVertex3f(c, -c, c); }
    //left
    { glNormal3f(-1.0F, 0.0F, 0.0F);
        glTexCoord2f(0.25F, 0.66F);
        glVertex3f(-c, c, -c);
        glTexCoord2f(0.25F, 0.34F);
        glVertex3f(-c, -c, -c);
        glTexCoord2f(0.00F, 0.34F);
        glVertex3f(-c, -c, c);
        glTexCoord2f(0.00F, 0.66F);
        glVertex3f(-c, c, c); }
    //right
    { glNormal3f(1.0F, 0.0F, 0.0F);
        glTexCoord2f(0.75F, 0.66F);
        glVertex3f(c, c, c);
        glTexCoord2f(0.75F, 0.34F);
        glVertex3f(c, -c, c);
        glTexCoord2f(0.50F, 0.34F);
        glVertex3f(c, -c, -c);
        glTexCoord2f(0.50F, 0.66F);
        glVertex3f(c, c, -c); }
    //down
    { glNormal3f(0.0F, -1.0F, 0.0F);
        glTexCoord2f(0.50F, 0.33F);
        glVertex3f(-c, -c, c);
        glTexCoord2f(0.50F, 0.00F);
        glVertex3f(-c, -c, -c);
        glTexCoord2f(0.25F, 0.0F);
        glVertex3f(c, -c, -c);
        glTexCoord2f(0.25F, 0.33F);
        glVertex3f(c, -c, c); }
    //up
    { glNormal3f(0.0F, 1.0F, 0.0F);
        glTexCoord2f(0.50F, 1.0F);
        glVertex3f(c, c, c);
        glTexCoord2f(0.50F, 0.66F);
        glVertex3f(c, c, -c);
        glTexCoord2f(0.25F, 0.66F);
        glVertex3f(-c, c, -c);
        glTexCoord2f(0.25F, 1.00F);
        glVertex3f(-c, c, c); }

    glEnd();
    glPopMatrix();

}

/**
* @param ct longueur du côté du cube
* @brief Permet de créer un cube de côté ct sans la aprtie supérieur. Principalement utilisé pour dessiner le wagon 
*/
static void mySolidCubeAvecNormalesSansUp(double ct) {
    float c = (float)ct / 2.0F;
    glPushMatrix();
    glBegin(GL_QUADS);

    //front
    { glNormal3f(0.0F, 0.0F, -1.0F);
        glTexCoord2f(0.50F, 0.66F);
        glVertex3f(c, c, -c);
        glTexCoord2f(0.50F, 0.34F);
        glVertex3f(c, -c, -c);
        glTexCoord2f(0.25F, 0.34F);
        glVertex3f(-c, -c, -c);
        glTexCoord2f(0.25F, 0.66F);
        glVertex3f(-c, c, -c); }
    //back
    { glNormal3f(0.0F, 0.0F, 1.0F);
        glTexCoord2f(0.75F, 0.66F);
        glVertex3f(c, c, c);
        glTexCoord2f(1.0F, 0.66F);
        glVertex3f(-c, c, c);
        glTexCoord2f(1.0F, 0.34F);
        glVertex3f(-c, -c, c);
        glTexCoord2f(0.75F, 0.34F);
        glVertex3f(c, -c, c); }
    //left
    { glNormal3f(-1.0F, 0.0F, 0.0F);
        glTexCoord2f(0.25F, 0.66F);
        glVertex3f(-c, c, -c);
        glTexCoord2f(0.25F, 0.34F);
        glVertex3f(-c, -c, -c);
        glTexCoord2f(0.00F, 0.34F);
        glVertex3f(-c, -c, c);
        glTexCoord2f(0.00F, 0.66F);
        glVertex3f(-c, c, c); }
    //right
    { glNormal3f(1.0F, 0.0F, 0.0F);
        glTexCoord2f(0.75F, 0.66F);
        glVertex3f(c, c, c);
        glTexCoord2f(0.75F, 0.34F);
        glVertex3f(c, -c, c);
        glTexCoord2f(0.50F, 0.34F);
        glVertex3f(c, -c, -c);
        glTexCoord2f(0.50F, 0.66F);
        glVertex3f(c, c, -c); }
    //down
    { glNormal3f(0.0F, -1.0F, 0.0F);
        glTexCoord2f(0.50F, 0.33F);
        glVertex3f(-c, -c, c);
        glTexCoord2f(0.50F, 0.00F);
        glVertex3f(-c, -c, -c);
        glTexCoord2f(0.25F, 0.0F);
        glVertex3f(c, -c, -c);
        glTexCoord2f(0.25F, 0.33F);
        glVertex3f(c, -c, c); }

    glEnd();
    glPopMatrix();

}

/**
* @param hauteur hauteur du cylindre
* @param rayon rayon du cylindre
* @param ns nombre de facette horizontale
* @param nl nombre de facettes verticale
* @param bases détermine si on veut afficher des bases
* @brief Permet de créer un cube de côté ct. Principalement utilisé pour palcer les textures de la skybox
*/
static void mySolidCylindre(double hauteur, double rayon, int ns, int nl, int bases) {
    GLboolean nm = glIsEnabled(GL_NORMALIZE);
    if (!nm)
        glEnable(GL_NORMALIZE);
    float normale[4];
    glGetFloatv(GL_CURRENT_NORMAL, normale);
    glPushMatrix();
    for (int j = 0; j < nl; j++) {
        float hi = hauteur / 2 - j * hauteur / nl;
        float hf = hi - hauteur / nl;
        glBegin(GL_QUAD_STRIP);
        for (int i = 0; i <= ns; i++) {
            float a = (2 * M_PI*i) / ns;
            float cs = cos(a);
            float sn = -sin(a);
            glNormal3f(cs, 0.0F, sn);
            float x = rayon * cs;
            float z = rayon * sn;
            glVertex3f(x, hi, z);
            glVertex3f(x, hf, z);
        }
        glEnd();
    }
    if (bases) {
        glBegin(GL_POLYGON);
        glNormal3f(0.0F, 1.0F, 0.0F);
        for (int i = 0; i < ns; i++) {
            float a = (2 * M_PI*i) / ns;
            float cs = cos(a);
            float sn = -sin(a);
            float x = rayon * cs;
            float z = rayon * sn;
            glVertex3f(x, hauteur / 2.0F, z);
        }
        glEnd();
        glBegin(GL_POLYGON);
        glNormal3f(0.0F, -1.0F, 0.0F);
        for (int i = 0; i < ns; i++) {
            float a = (2 * M_PI*i) / ns;
            float cs = cos(a);
            float sn = sin(a);
            float x = rayon * cs;
            float z = rayon * sn;
            glVertex3f(x, -hauteur / 2.0F, z);
        }
        glEnd();
    }
    glPopMatrix();
    glNormal3f(normale[0], normale[1], normale[2]);
    if (!nm)
        glDisable(GL_NORMALIZE);
}

/**
* @param taille coefficient pour la taille de l'ourson
* @brief Permet de créer un ourson proportionnelle avec la taille donnée en paramètre
*/
static void myPassenger(double taille) {

    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, ours);

    // Modélisation du corps
    glPushMatrix();
    glTranslatef(0.0F, -0.8F*(taille / 2), 0.0F);
    glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
    glutSolidCone(0.4F*taille, 1.2F*taille, 20, 20);
    glPopMatrix();

    // Bras
    glPushMatrix();
    glTranslatef(0.2F*taille, 0.8F*taille / 2, 0.0F);
    glRotatef(-45.0F, 0.0F, 0.0F, 1.0F);
    mySolidCylindre(0.8F*taille, 0.08F*taille, 20, 20, true);
    glPopMatrix();

    // Bras
    glPushMatrix();
    glTranslatef(-0.2F*taille, 0.8F*taille / 2, 0.0F);
    glRotatef(45.0F, 0.0F, 0.0F, 1.0F);
    mySolidCylindre(0.8F*taille, 0.08F*taille, 20, 20, true);
    glPopMatrix();

    // "Main"
    glPushMatrix();
    glTranslatef(0.45F*taille, 1.3F*taille / 2, 0.0F);
    glutSolidSphere(0.13F*taille, 20, 20);
    glPopMatrix();

    // "Main"
    glPushMatrix();
    glTranslatef(-0.45F*taille, 1.3F*taille / 2, 0.0F);
    glutSolidSphere(0.13F*taille, 20, 20);
    glPopMatrix();

    // Modélisation de la tête
    glPushMatrix();

    glTranslatef(0.0F, 0.8F*taille, 0.0F);
    glutSolidSphere(0.25F*taille, 20, 20);

    // Oreille
    glPushMatrix();
    glTranslatef(0.15F*taille, 0.15F*taille, 0.0F);
    glutSolidTorus(0.05F*taille, 0.1F*taille, 20, 20);
    glScalef(1.0F, 1.0F, 0.7F);
    glutSolidSphere(0.1F*taille, 20, 20);
    glPopMatrix();

    // Oreille
    glPushMatrix();
    glTranslatef(-0.15F*taille, 0.15F*taille, 0.0F);
    glutSolidTorus(0.05F*taille, 0.1F*taille, 20, 20);
    glScalef(1.0F, 1.0F, 0.7F);
    glutSolidSphere(0.1F*taille, 20, 20);
    glPopMatrix();

    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, noir);

    // Oeil
    glPushMatrix();
    glTranslatef(0.07F*taille, 0.02F*taille, 0.21F*taille);
    glScalef(0.8F, 1.0F, 0.6F);
    glutSolidSphere(0.06F*taille, 10, 10);
    glPopMatrix();

    // Oeil
    glPushMatrix();
    glTranslatef(-0.07F*taille, 0.02F*taille, 0.21F*taille);
    glScalef(0.8F, 1.0F, 0.6F);
    glutSolidSphere(0.06F*taille, 10, 10);
    glPopMatrix();

    // Nez
    glPushMatrix();
    glTranslatef(0.0F, -0.05F*taille, 0.27F*taille);
    glScalef(1.0F, 0.6F, 0.6F);
    glutSolidSphere(0.05F*taille, 20, 20);
    glPopMatrix();

    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, beige);

    // Museau
    glPushMatrix();
    glTranslatef(0.0F, -0.1F*taille, 0.2F*taille);
    glRotatef(20.0F, 1.0F, 0.0F, 0.0F);
    glScalef(1.0F, 1.0F, 0.6F);
    glutSolidSphere(0.12F*taille, 10, 10);
    glPopMatrix();

    glPopMatrix();
}

static void myWagon(double taille) {

    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, wagon);

    glPushMatrix();
    glScalef(1.0F, 0.8F, 1.5F);
    mySolidCubeAvecNormalesSansUp(taille);
    glPopMatrix();

    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, gris);

    glPushMatrix();

    // Roue
    glPushMatrix();
    glTranslatef(0.5F*taille, -0.45F*taille, -0.4F*taille);				// (écartement, hauteur, longueur)
    glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
    glRotatef(rotate, 0.0F, 1.0F, 0.0F);
    mySolidCylindre(0.1F*taille, 0.15F*taille, 20, 5, true);
    glPopMatrix();

    // Roue
    glPushMatrix();
    glTranslatef(0.5F*taille, -0.45F*taille, 0.4F*taille);
    glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
    glRotatef(rotate, 0.0F, 1.0F, 0.0F);
    mySolidCylindre(0.1F*taille, 0.15F*taille, 20, 5, true);
    glPopMatrix();

    // Roue
    glPushMatrix();
    glTranslatef(-0.5F*taille, -0.45F*taille, -0.4F*taille);
    glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
    glRotatef(rotate, 0.0F, 1.0F, 0.0F);
    mySolidCylindre(0.1F*taille, 0.15F*taille, 20, 5, true);
    glPopMatrix();

    // Roue
    glPushMatrix();
    glTranslatef(-0.5F*taille, -0.45F*taille, 0.4F*taille);
    glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
    glRotatef(rotate, 0.0F, 1.0F, 0.0F);
    mySolidCylindre(0.1F*taille, 0.15F*taille, 20, 5, true);
    glPopMatrix();

    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, noir);

    // Phare
    glPushMatrix();
    glTranslatef(0.2F*taille, 0.1F*taille, 1.6*taille / 2);
    glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
    mySolidCylindre(0.1F*taille, 0.13F*taille, 20, 5, true);
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, jaune);
    mySolidCylindre(0.11F*taille, 0.1F*taille, 20, 5, true);
    glPopMatrix();

    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, noir);

    // Phare
    glPushMatrix();
    glTranslatef(-0.2F*taille, 0.1F*taille, 1.6*taille / 2);
    glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
    mySolidCylindre(0.1F*taille, 0.13F*taille, 20, 5, true);
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, jaune);
    mySolidCylindre(0.11F*taille, 0.1F*taille, 20, 5, true);
    glPopMatrix();

    glPopMatrix();

    myPassenger(taille);
}

/**
* @brief Permet de créer la skybox
*/
static void drawFloor2() {
    glEnable(GL_TEXTURE_2D);
    glPushMatrix();
    mySolidCubeAvecNormales(40.0);
    glPopMatrix();
    glDisable(GL_TEXTURE_2D);
}

static void scene(void) {
    drawFloor2();
    Dir3D tangente,Y(0,1,0),res;
    Pos3D leftPoint0, leftPoint1, rightPoint0, rightPoint1;
    double angle;
    static const float color[4]={0.7f,0.7f,0.7f,1.0f};
    static const float marron[4] = { 0.063f,0.034f,0.004f,1.0f };


    for ( int i = 0 ; i < nb-1 ; i++ ) {
        // Calcul de la tangente en splinePoints[i]
        tangente.x = splinePoints[(i+1)%nb]->x - splinePoints[i]->x;
        tangente.y = splinePoints[(i+1)%nb]->y - splinePoints[i]->y;
        tangente.z = splinePoints[(i+1)%nb]->z - splinePoints[i]->z;
        tangente.normalisation();

        Y.x = splinePoints[i]->x-splinePointsVerticales[i]->x;
        Y.y = splinePoints[i]->y-splinePointsVerticales[i]->y;
        Y.z = splinePoints[i]->z-splinePointsVerticales[i]->z;
        Y.normalisation();
        //leftpoint0 = splinePoint[0] + 0.3 * tangente ^ Y;
        Dir3D::produitVectoriel(&tangente,&Y,&res);
        //res.normalisation();
        leftPoint0.x = splinePoints[i]->x + 0.15 * res.x;
        leftPoint0.y = splinePoints[i]->y + 0.15 * res.y;
        leftPoint0.z = splinePoints[i]->z + 0.15 * res.z;

        rightPoint0.x = splinePoints[i]->x - 0.15 * res.x;
        rightPoint0.y = splinePoints[i]->y - 0.15 * res.y;
        rightPoint0.z = splinePoints[i]->z - 0.15 * res.z;


        tangente.x = splinePoints[(i+2)%nb]->x - splinePoints[(i+1)%nb]->x;
        tangente.y = splinePoints[(i+2)%nb]->y - splinePoints[(i+1)%nb]->y;
        tangente.z = splinePoints[(i+2)%nb]->z - splinePoints[(i+1)%nb]->z;
        tangente.normalisation();

        Y.x = splinePoints[i+1]->x-splinePointsVerticales[i+1]->x;
        Y.y = splinePoints[i+1]->y-splinePointsVerticales[i+1]->y;
        Y.z = splinePoints[i+1]->z-splinePointsVerticales[i+1]->z;
        Y.normalisation();
        // PG1 = splinePoint[i+1] + 0.3 * tangente ^ Y
        Dir3D::produitVectoriel(&tangente, &Y, &res);
        //res.normalisation();
        leftPoint1.x = splinePoints[(i+1)%nb]->x + 0.15 * res.x;
        leftPoint1.y = splinePoints[(i+1)%nb]->y + 0.15 * res.y;
        leftPoint1.z = splinePoints[(i+1)%nb]->z + 0.15 * res.z;

        rightPoint1.x = splinePoints[(i+1)%nb]->x - 0.15 * res.x;
        rightPoint1.y = splinePoints[(i+1)%nb]->y - 0.15 * res.y;
        rightPoint1.z = splinePoints[(i+1)%nb]->z - 0.15 * res.z;


        drawPoint(&leftPoint0,&leftPoint1,0.05);
        drawPoint(&rightPoint0,&rightPoint1,0.05);

        // poteau
        if((i % 2 == 0) && (i < 54 || i > 68)) {
            glPushMatrix();
            glTranslated(leftPoint0.x, 0, leftPoint0.z);
            glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
            drawCylinder(0.07, splinePoints[i]->y, 6);
            glPopMatrix();
            glPushMatrix();
            glTranslated(rightPoint0.x, 0, rightPoint0.z);
            glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
            drawCylinder(0.07, splinePoints[i]->y, 6);
            glPopMatrix();
        }

        Dir3D cZ(splinePoints[(i+1)%nb]->x-splinePoints[i]->x,
                 splinePoints[(i+1)%nb]->y-splinePoints[i]->y,
                 splinePoints[(i+1)%nb]->z-splinePoints[i]->z);

        cZ.normalisation();
        // calcul du vecteur vertical
        Dir3D cY(splinePointsVerticales[i]->x-splinePoints[i]->x,
                 splinePointsVerticales[i]->y-splinePoints[i]->y,
                 splinePointsVerticales[i]->z-splinePoints[i]->z);

        cY.normalisation();
        Dir3D cX;
        Dir3D::produitVectoriel(&cY,&cZ,&cX);
        cX.normalisation();

        Dir3D::produitVectoriel(&cZ,&cX,&cY);
        cY.normalisation();


        GLfloat mat[16];
        mat[0] = cX.x;
        mat[1] = cX.y;
        mat[2] = cX.z;
        mat[3] = 0.0f;
        mat[4] = cY.x;
        mat[5] = cY.y;
        mat[6] = cY.z;
        mat[7] = 0.0f;
        mat[8] = cZ.x;
        mat[9] = cZ.y;
        mat[10] = cZ.z;
        mat[11] = 0.0f;
        mat[12] = 0.0f;
        mat[13] = 0.0f;
        mat[14] = 0.0f;
        mat[15] = 1.0f;


        glPushMatrix();
        glTranslated(splinePoints[i]->x,splinePoints[i]->y,splinePoints[i]->z);
        glMultMatrixf(mat);
        glScalef(0.5,0.05,0.1);
        glMaterialfv(GL_FRONT,GL_AMBIENT_AND_DIFFUSE,marron);
        glutSolidCube(1);
        glPopMatrix();

    }

    glPushMatrix();
    glTranslated(currentTeapotPosition.x,currentTeapotPosition.y,currentTeapotPosition.z);
    glMaterialfv(GL_FRONT,GL_AMBIENT_AND_DIFFUSE,white);

    Dir3D cZ(currentTargetPosition.x-currentTeapotPosition.x,
             currentTargetPosition.y-currentTeapotPosition.y,
             currentTargetPosition.z-currentTeapotPosition.z);

    cZ.normalisation();
    // calcul du vecteur vertical
    Dir3D cY(currentTopPosition.x-currentTeapotPosition.x,
             currentTopPosition.y-currentTeapotPosition.y,
             currentTopPosition.z-currentTeapotPosition.z);

    cY.normalisation();
    Dir3D cX;
    Dir3D::produitVectoriel(&cY,&cZ,&cX);
    cX.normalisation();

    Dir3D::produitVectoriel(&cZ,&cX,&cY);
    cY.normalisation();


    GLfloat mat[16];
    mat[0] = cX.x;
    mat[1] = cX.y;
    mat[2] = cX.z;
    mat[3] = 0.0f;
    mat[4] = cY.x;
    mat[5] = cY.y;
    mat[6] = cY.z;
    mat[7] = 0.0f;
    mat[8] = cZ.x;
    mat[9] = cZ.y;
    mat[10] = cZ.z;
    mat[11] = 0.0f;
    mat[12] = 0.0f;
    mat[13] = 0.0f;
    mat[14] = 0.0f;
    mat[15] = 1.0f;

    glMultMatrixf(mat);
    glTranslatef(0.0f,0.25f,0.0f);
    myWagon(0.5);
    glPopMatrix();



    glMaterialfv(GL_FRONT,GL_AMBIENT_AND_DIFFUSE,white);

}

/* Fonction executee lors d'un rafraichissement */
/* de la fenetre de dessin                      */

static void display(void) {
    static float lightPos[4] = {0,30.0f,0,1.0};
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPushMatrix();
    gluLookAt(0.0,7.0,5.0,0.0,0.0,0.0,0.0,1.0,0.0);
    glRotatef(rx,1.0F,0.0F,0.0F);
    glRotatef(ry,0.0F,1.0F,0.0F);
    glRotatef(rz,0.0F,0.0F,1.0F);
    glTranslatef(0.0F,-2.5F,0.0F);

    glLightfv(GL_LIGHT0,GL_POSITION,lightPos);
    scene();
    glPopMatrix();
    glFlush();
    glutSwapBuffers();
    int error = glGetError();
    if ( error != GL_NO_ERROR )
        printf("Erreur OpenGL: %d\n",error);
}

static void display2(void) {
    static float lightPos[4] = {0,30.0f,0,1.0};
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPushMatrix();
    gluLookAt(currentTeapotPosition.x,currentTeapotPosition.y+1,currentTeapotPosition.z,currentTargetPosition.x,currentTargetPosition.y+1,currentTargetPosition.z,0.0,1.0,0.0);


    glLightfv(GL_LIGHT0,GL_POSITION,lightPos);
    scene();
    glPopMatrix();
    glFlush();
    glutSwapBuffers();
    int error = glGetError();
    if ( error != GL_NO_ERROR )
        printf("Erreur OpenGL: %d\n",error);
}

/* Fonction executee lors d'un changement       */
/* de la nbPoints de la fenetre OpenGL            */
/* -> Ajustement de la camera de visualisation  */

static void reshape(int tx,int ty) {
    glViewport(0,0,tx,ty);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    double ratio =(double) tx/ty;
    if ( ratio >= 1.0 )
        gluPerspective(80.0,ratio,0.1,50.0);
    else
        gluPerspective(80.0/ratio,ratio,0.1,50.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}


/* Fonction executee lors de la frappe          */
/* d'une touche du clavier                      */
static void keyboard(unsigned char key,int x,int y) {
    switch (key) {
        case 0x20 :
            aff = (aff+1)%5;
            break;
        case 0x1B : case 'q' :
            exit(0);
            break;
        case 'f' :
            glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
            break;
        case 'F' :
            glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
            break;

    }
    glutPostRedisplay();
}

static void clean(void) {
    for ( int i = 0 ; i < nbPoints ; i++ ) {
        delete(bfPos[i]);
        delete(bfPosVerticale[i]);

    }
    desallocationPos3D(splinePoints,nb);
	desallocationPos3D(posVitessePoints,nbPointVitesse);
	isInit = false;
}

static void idle(void){
    static int currentTime = 0;
    int t = glutGet(GLUT_ELAPSED_TIME);
    double dt = (t - currentTime)/1000.0;
    currentTime = t;

    rotate--;

    section += dt*0.5; // vitesse initialisée à 0.5 u/s.

    int progression = (int)section;
    if(progression > nbPoints - 4){
        section -= nbPoints - 3;
        progression = 0;
    }

    // update teapot position
    determinationPositionSurBSpline(&bfPos[progression],section-progression,CATMULL_ROM,&currentTeapotPosition);

    double s = section +0.1*dt;// plus loin
    progression = (int)s;
    if(progression > nbPoints - 4){
        s -= nbPoints - 4;
        progression = 0;
    }


    determinationPositionSurBSpline(&bfPos[progression],s-progression,CATMULL_ROM,&currentTargetPosition);

    determinationPositionSurBSpline(&bfPosVerticale[progression],s-progression,CATMULL_ROM,&currentTopPosition);



    postRedisplay();
}

/* Fonction principale                          */

int main(int argc,char **argv) {

    atexit(clean);

    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_RGBA|GLUT_DEPTH|GLUT_DOUBLE);
    glutInitWindowSize(800,450);
    glutInitWindowPosition(50,50);
    f1 = glutCreateWindow("Fenêtre 1");
    init();
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(special);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutDisplayFunc(display);
    glutInitWindowSize(800,450);
    glutInitWindowPosition(50,500);
    f2 = glutCreateWindow("Fenêtre 2");
    init();
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(special);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutDisplayFunc(display2);
    glutIdleFunc(idle);
    glutMainLoop();

    return(0);
}