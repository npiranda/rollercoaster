/**
 * \file CH3D.cpp
 * \brief Ce fichier contient les opérations arithmétiques sur les coordonnées 3D
 * \authors Emile Eid
 * \authors Camille Idatte
 * \authors Nathan Piranda
 */

#include "../include/CH3D.h"
#include <stdio.h>


CH3D::CH3D(void) {
    c[0] = 0;
    c[1] = 0;
    c[2] = 0;
    c[3] = 1;
}

CH3D::CH3D(double x, double y, double z, double t) {
    c[0] = x;
    c[1] = y;
    c[2] = z;
    c[3] = t;
}

CH3D::CH3D(CH3D *clone) {
    c[0] = clone->c[0];
    c[1] = clone->c[1];
    c[2] = clone->c[2];
    c[3] = clone->c[3];

}

void CH3D::print() {
    printf("%lf \t %lf \t %lf \t %lf \n ",c[0], c[1], c[2], c[3]);
}

CH3D::~CH3D() {
}