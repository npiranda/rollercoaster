/**
 * \file Pos3D.cpp
 * \brief Ce fichier contient les opérations arithmétiques sur les positions 3D.
 * \authors Emile Eid
 * \authors Camille Idatte
 * \authors Nathan Piranda
 */
#include <math.h>
#include "../include/Pos3D.h"

Pos3D::Pos3D(void):CH3D(0.0,0.0,0.0,1.0) {
}
 
Pos3D::Pos3D(double px, double py, double pz):CH3D(px,py,pz,1.0) {
}

Pos3D::Pos3D(Pos3D *p):CH3D(p) {
}

Pos3D::~Pos3D() {

}

double Pos3D::distance(Pos3D *p) {
    return (sqrt((
                         pow((p->c[0])-(this->c[0]),2.0))+(
                         pow((p->c[1])-(this->c[1]),2.0))+(
                         pow((p->c[2])-(this->c[2]),2.0))));
}

double Pos3D::distance(Pos3D *p1,Pos3D *p2) {
    return (sqrt((
                         pow((p2->c[0])-(p1->c[0]),2.0))+(
                         pow((p2->c[1])-(p1->c[1]),2.0))+(
                         pow((p2->c[2])-(p1->c[2]),2.0))));
}