# Projet Grand Huit

##### Credits
- Emile Eid
- Idatte Camille
- Piranda Nathan

##### Files
- `Scene.cpp` : execute the main scene of the project

##### C++ Classes
- `CH3D.cpp` : contains the arithmetic operations about 3D coordinates
- `Dir3D.cpp` : contains the arithmetic operations about 3D directions
- `Pos3D.cpp` : contains the arithmetic operations about 3D positions.


